﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Data;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace FileExplorer;
/// <summary>
/// The partial class <c>MainWindowViewModel</c> is written with the new .NET community toolkit 8.0.
/// The class contains the <c>_directoriesNames</c> and <c>InputDirectory</c> properties which are bound to the UI. 
/// Additionally the class contains also a object (`_itemsLock`) for the <c>EnableCollectionSynchronization</c>-implementation.
/// Finally we also have a <c>[RelayCommand]</c> async method which is bound on the button in the UI.
/// </summary>
public partial class MainWindowViewModel : ObservableObject
{
    /// <summary>
    /// <c>_directoriesNames</c> is bound on <c>ListViewDirectories</c>
    /// </summary>
    [ObservableProperty]
    private ObservableCollection<string> _fileNames;
    /// <summary>
    /// <c>InputDirectoryPath</c> is bound on <c>TextBoxInputField</c>
    /// </summary>
    public string? InputDirectoryPath
    {
        get; set;
    }

    /// <summary>
    /// <c>_itemsLock</c> is necessary to implement the <c>BindingOperations.EnableCollectionSynchronization</c> for the <c>DirectoriesNames</c>
    /// </summary>
    private static readonly object _itemsLock = new();

    /// <summary>
    /// The constructor of <c>MainWindowViewModel</c> initializes the ObservableCollection and
    /// also enables the CollectionSynchronization. This is necessary that the async method (another thread) 
    /// can update the UI. 
    /// </summary>
    public MainWindowViewModel()
    {
        Logging.DebugOutput("Entering the constructor of MainWindowViewModel.");
        FileNames = new ObservableCollection<string>();
        BindingOperations.EnableCollectionSynchronization(FileNames, _itemsLock);
        Logging.DebugOutput("Leaving the constructor of MainWindowViewModel.");
    }

    /// <summary>
    /// The <c>ClickButtonListDirectory</c> is a <c>async</c> method to run the <c>Model.ListDirectory</c> method. 
    /// If the <c>Model.ListDirectory</c> return something we'll loop over the results and add it to the <c>DirectoriesNames</c>. 
    /// <c>DirectoriesNames represents the <c>ListViewDirectories</c> (via binding).</c>
    /// </summary>
    [RelayCommand]
    private async Task ListDirectory()
    {
        Logging.DebugOutput("The button was clicked.");
        await Task.Run(() =>
        {
            try
            {
                var getDirs = Model.GetDirectoryFiles(InputDirectoryPath);
                Logging.DebugOutput("The Model.ListDirectory method returned something.");
                foreach (var dir in getDirs)
                {
                    FileNames.Add(dir);
                    Logging.DebugOutput("DirectoriesNames added: " + dir.ToString());
                }
            }
            catch (Exception e)
            {
                Logging.DebugOutput(e.Message);
            }
        });
    }
}
