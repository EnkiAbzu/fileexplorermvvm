﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using CommunityToolkit.Diagnostics;

namespace FileExplorer;
internal static class Model
{
    /// <summary>
    /// The ListDirectory method list all files within a directory. 
    /// </summary>
    /// <param name="directoryPath">The parameter contains the string of the <c>TextBoxInputField</c>.</param>
    /// <returns name="List&lt;string&gt;">The list contains all files within the given directory.</returns>
    public static List<string> GetDirectoryFiles(string? directoryPath)
    {
        Guard.IsNotNullOrEmpty(directoryPath, nameof(directoryPath));

        Logging.DebugOutput("Intense work starts now.");
        Thread.Sleep(5_000); //simulating cpu intense work... 
        Logging.DebugOutput("Intense work finished.");

        DirectoryInfo directory = new DirectoryInfo(directoryPath);
        return directory.GetFiles().Select(x => x.Name).ToList();
    }
}
