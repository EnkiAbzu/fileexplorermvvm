﻿using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace FileExplorer;
internal static class Logging
{
    /// <summary>
    /// A small helper function to wrap the <c>Trace.WriteLine</c> method to get the correct Caller.
    /// </summary>
    /// <param name="debugText">the text you want to print to the debugger session</param>
    /// <param name="caller">is an optional parameter and usually takes the CallerMemberName for better tracebility</param>
    public static void DebugOutput(string debugText, [CallerMemberName] string caller = "")
    {
        Trace.WriteLine($"{caller} : {debugText}");
    }
}
