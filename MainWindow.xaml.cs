﻿using System.Windows;

namespace FileExplorer;
/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    /// <summary>
    /// The <c>MainWindow</c> class is the code behind file of MainWindow.xaml. 
    /// The only necessary thing to setup is the DataContext. 
    /// </summary>
    public MainWindow()
    {
        InitializeComponent();
        DataContext = new MainWindowViewModel();
    }
}
